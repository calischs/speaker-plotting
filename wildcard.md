# Plotting membrane speakers

## Overview

Project for wire plotting of membrane speakers.  for wildcard week in mas.836 2018

- Sam Calisch
- Elena Chong
- Zijun Wei
- Victoria Shen

We will use a custom magnet wire plotting tool for the Zund cutter to plot coils for audio speakers.

<img src="https://calischs.pages.cba.mit.edu/kiri-tww/img/flatcoil/flatcoil-40awg-small.mp4" width=600px>

<img src="https://calischs.pages.cba.mit.edu/kiri-tww/img/flatcoil/flatcoil-40awg-microscope.jpg" height=250px>
<img src="https://gitlab.cba.mit.edu/calischs/kiri-tww/raw/master/img/flatcoil/flatcoil-cut.jpg" height=250px>

## TODO

- ~~waterjet steel layers~~
- ~~laser cut shim layer (500um? 1mm?). (elena)~~
- test heat+vacuum for membrane layers (sam)
- ~~verify magnet orientations with gaussmeter.~~
- ~~modify coil cutout to extend beyond frame for easy stretching, run job (sam)~~
- ~~solder leads (mount RCA jack?) (victoria)~~
- ~~test with audio amp~~

optional
- do a version with 1/16" magnets and 1/16" phenolic layer
- do a couple more iteration and polish final product


## Simulation

Two approaches: Coil-magnet, and coil-coil.

### Coil-coil

The coil-coil topology relies on coils to generate all magnetic fields, not just the time varying one.


<a href='comsol/coil-coil-speaker.mph'>comsol file</a>

<img src="img/coil-coil-flux.png" height=200px>

<img src="img/coil-coil-force.png" height=200px>

### Magnet-coil

The coil-magnet topology (planar magnetic) is used for some high end headphones.  It can produce significantly more force than coil-coil, even with very thin magnets.

<a href='comsol/magnet-coil-speaker.mph'>comsol file</a>

<img src="img/magnet-coil-flux.png" height=300px>
<img src="img/magnet-coil-force.png" height=300px>

#### magnet grids

In our meeting this morning, we talked about how rod magnets are more widely available than long, thin bar magnets, and that making grids of rod magnets could make assembly really easy and offer design freedom.  Below are two sketches of square and hex grids with coils routed to capture perpendicular flux.  Magnets are blue circles, all closest pairs have opposite polarity.

<img src="img/square-grid.png" height=400px>
<img src="img/hex-grid.png" height=400px>

The design below has rotational symmetry, which may help flatten its frequency response.

<img src="img/hex-spiral-screenshot.png" height=400px>
<img src="img/hex-spiral-coil.jpg" height=400px>

Watching the coils being plotted is fun:

<img src="img/hex-spiral-plotting.mp4" height=400px>

## Design and Fabrication

### Long coil for testing

First, we decided to start off with making a rectangular frame for clamping firmly the membrane (wire plotting on 25 micron kapton tape) that Sam initially designed for another project of his.

<img src= "img/wireplotting1.jpg" height=400px>
<img src= "img/wireplotting2.jpg" height=400px>  

The initial frame that we designed contained pockets on each side for placing the "rubber" that will create friction between the frame plates. 

<img src= "img/wireplotting4.jpg" height=400px>  

We used the Epilog Lasercutter to lasercut the frames on 1/8" acrylic sheet.  

<img src= "img/wireplotting3.jpg" height=400px>  

For the pocket, we used 60% speed, 100% power, and 200ppi, for the outline of the frame we used 3% speed, 100% power, and 200ppi.  

<img src= "img/wireplotting5.jpg" height=400px>

One of the resulting frame looked like the picture below:

<img src= "img/wireplotting6.jpg" height=400px>  

We used Crazy Glue to glue the rubber in place.  

<img src= "img/wireplotting7.jpg" height=400px>

For the first attempt, we just placed the membrane between the two plates and used a few M4 screws to clamp it.

<img src= "img/wireplotting8.jpg" height=400px>

We noticed that the acrylic is not a good material for exerting pressure. It started bending, which defied its purpose.

<img src= "img/wireplotting9.jpg" height=400px>

A bird view of membrane clamped with the long rectangular frame.

<img src= "img/wireplotting10.jpg" height=400px>

### Magnet-Coil Hexagonal design

After brainstorming for the first design iteration, we decided on using magnet and coil with Sam's idea of a magnet grid. He succesfully got a few plotted out nicely on 25 micron kapton tape.  

<img src= "img/wireplotting11.0.jpg" height=400px>

<!--need to verify if it is 1/8" magnet and wood... was it plywood?-->
We then lasercut the outer frame from 1/8" wood to clamp the coil membrane. 

<img src= "img/wireplotting11.jpg" height=400px>

We then lasercut the magnet grid plate from the same kind of wood.

<img src= "img/wireplotting12.jpg" height=400px>

Then, 1/8" magnets were placed in the holes from the magnet-grid plate. Each grid has the opposite polarity.  

<img src= "img/wireplotting13.jpg" height=400px>

<!--Just for fun, we placed a MAGNETIC SHEET?????????????? FORGOT NAME OF THE PAPER AND ITS FUNCTION-->
<!--<img src= "img/wireplotting14.jpg" height=400px>-->

We used a Gaussmeter to check for the magnets polarity. 

<img src= "img/wireplotting17.jpg" height=400px>

Here is the mapped out magnet-grid. Green for positive and black for negative polarity. We later spray painted this plate with yellow paint to cover the drawing.

<img src= "img/wireplotting18.jpg" height=400px>

Sam used the water-jet to cut out a steel plate for the ground plate (the base).

<img src= "img/wireplotting19.jpg" height=400px>

<img src= "img/wireplotting20.jpg" height=400px>

We tried to lasercut some of the materials that Sam provided us. We tried to cut the 1/8" and 1/16" phenolic sheets with the lasercutter using 0.7% speed, 100% power, 400ppi and were not successful, so we decided to just do it on the plastic sheet.

<img src= "img/wireplotting21.jpg" height=400px>

It did not even go through the sheet! Here is a picture of the burned phenolic sheet at different speed level (0.7-1.5%).

<img src= "img/wireplotting22.jpg" height=400px>

Sam also provided us with a very thin plastic sheet.

<img src= "img/wireplotting23.jpg" height=400px>

We tested the cutting settings and found that the best values were: 3% speed, 80-85% power, 400 ppi.

<img src= "img/wireplotting24.jpg" height=400px>

Here are some five shim/spacers we lasercut from this thin plastic sheet.

<img src= "img/wireplotting25.jpg" height=400px>

During our design discussing, we did not get that far ahead into thinking about the testing process. We forgot to add clearance for an RCA plug which will be used for sending audio signals to the coil. Hence, we had to separate the magnet-grid plate from the steel plate (We snapped them together beforehand.)

<img src= "img/wireplotting26.jpg" height=400px>

We used the drill to create a bigger hole for the RCA plug. Had to do it for all layers: steel plate, magnet-grid plate, spacers/shims, frame plate.

<img src= "img/wireplotting27.jpg" height=400px>

Some fine soldering work came next. Had to peel off the insulation layer from the copper wire for soldering. The copper used for plotting was 40 gauge which makes it difficult to connect to the RCA plug. Thus, we stepped up the copper wire by soldering it to a thicker one.

<img src= "img/wireplotting28.jpg" height=400px>

The resulting planar speaker.

<img src= "img/wireplotting29.jpg" height=400px>

<img src= "img/wireplotting30.jpg" height=400px>

## Testing

It is testing time!

<img src= "img/wireplotting31.jpg" height=400px>

We tested our first iteration and here are the results:

No spacer/shim. Minimum separation between the coil membrane and the magnets plate. (First Reaction!)

<img src="img/out_noshim.mp4" height=400px>

With two spacers separation between the coil membrane and the magnets plate.

<img src="img/out_twospacers.mp4" height=400px>

Three spacers separation between the coil membrane and the magnets plate. This video also shows some of the layers of this planar speaker.

<img src="img/out_threespacers.mp4" height=400px>

First iteration was a success!

## Stereo Planar Speaker

We made some more speakers today.

<img src="img/wireplotting-101.jpg" height=400px>

All the parts of one planar speaker.

<img src="img/week13-02.jpg" height=400px>

Coil membrane of the speaker.

<img src="img/wireplotting-100.jpg" height=400px>

An assembled planar speaker.

<img src="img/week13-01.jpg" height=400px>

Test of the stereo speaker with coil membrane pasted on the plastic. 
<img src="img/output.mp4" height=400px>

If you compare this video with the two spacers one, you will notice that it seems to have better sound with a stiffer coil membrane.

The speaker on the right has some holes that were countersinked so we could nest the bolts deeper into the top frame and hid the holes with a plastic film so only the RCA jack is exposed.

## Links:

- [Planar magnetic headphones](https://www.innerfidelity.com/content/how-planar-magnetic-headphones-work)
- [Ben Katz's Planar magnetic headphones pt. 1](http://build-its-inprogress.blogspot.com/2017/08/planar-magnetic-headphones-part-1.html)
- [Ben Katz's Planar magnetic headphones pt. 2](http://build-its-inprogress.blogspot.com/2017/09/planar-magnetic-headphones-part-2.html)
- [Applied Science magnetics video](https://www.youtube.com/watch?v=4UFKl9fULkA)